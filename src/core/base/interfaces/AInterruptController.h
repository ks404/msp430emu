//
// Created by Ignaty Deltsov on 01/11/16.
//

#ifndef MSP430EMU_INTERRUPT_CONTROLLER_H
#define MSP430EMU_INTERRUPT_CONTROLLER_H

#include "APeripheral.h"
#include "ADeviceComponent.h"
#include "AMemoryRegister.h"
#include <list>

namespace vmc {

    class AInterruptController : public ADeviceComponent {
    public:

        virtual bool processInterrupt() = 0;

    };

    typedef std::shared_ptr<AInterruptController> PInterruptController;
}

#endif //MSP430EMU_INTERRUPT_CONTROLLER_H

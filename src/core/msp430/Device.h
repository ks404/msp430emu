//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MPS430_DEVICE_H
#define MSP430EMU_MPS430_DEVICE_H

#include "hardware/CPU.h"
#include "hardware/SystemMemory.h"
#include "hardware/SystemDecoder.h"
#include "hardware/SystemTimer.h"
#include "../base/interfaces/ADevice.h"

namespace msp430 {
    class Device : public vmc::ADevice {
    public:
        Device(int ramSize, int romSize) : vmc::ADevice(
                std::make_shared<CPU>(),
                std::make_shared<SystemMemory>(ramSize, romSize),
                nullptr,
                std::make_shared<SystemTimer>(),
                std::make_shared<SystemDecoder>()
        ) { }

        bool init() override {
            auto iam = this->getptr();
            cpu->device(iam);
            memory->device(iam);
            timer->device(iam);
            decoder->device(iam);
            cpu->pc = memory->loadWord(0x0); // load from reset IVT
            return true;
        }
        // TODO: ADD PC PROCESSING
        // TODO: ADD FLAG PROCESSING
    };
}
#endif //MSP430EMU_MPS430_DEVICE_H

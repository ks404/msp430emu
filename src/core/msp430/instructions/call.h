//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_CALL_H
#define MSP430EMU_CALL_H

#include "../../base/interfaces/ADevice.h"
#include "../operands/Register.h"
#include "../operands/Void.h"

namespace msp430 {
    class Call : public vmc::AInstruction {
    public:
        Call(const vmc::PAOperand vop1, const int size) :
                AInstruction("call", vop1, std::make_shared<Void>(), size) { }

        bool execute(vmc::PDevice dev) override {
            auto sp = Register::SP->value(dev);
            auto pc = Register::PC->value(dev);
            sp -= 2;
            dev->memory->storeWord(sp, pc);
            Register::SP->value(dev, sp);
            auto addr = op1->value(dev);
            Register::PC->value(dev, addr);
            return true;
        }
    };
}

#endif //MSP430EMU_CALL_H

//
// Created by ALG on 01.11.2016.
//

#ifndef MSP430EMU_RRC_H
#define MSP430EMU_RRC_H
#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Rrc : public vmc::AInstruction {
    public:
        Rrc(const vmc::PAOperand vop1) :
                AInstruction("rrc", vop1, std::make_shared<Void>(), size) { }

        bool execute(vmc::PDevice dev) override {
            auto lsb = op1->bit(dev, 0);
            auto msb = Register::SR->bit(dev, STATUS_C);
            auto dst = (op1->value(dev))>>1;
            if (op1->size == vmc::AOperand::BYTE)
            {
                auto sign = op1->bit(dev, 6);
                dst|=msb<<7;
                if (~sign&&msb)
                    Register::SR->bit(dev, STATUS_V, 1);
                else Register::SR->bit(dev, STATUS_V, 0);
                if (dst==0x00)
                    Register::SR->bit(dev, STATUS_Z, 1);
                else Register::SR->bit(dev, STATUS_Z, 0);
            }
            if (op1->size == vmc::AOperand::WORD)
            {
                auto sign = op1->bit(dev, 14);
                dst|=msb<<15;
                if (~sign&&msb)
                    Register::SR->bit(dev, STATUS_V, 1);
                else Register::SR->bit(dev, STATUS_V, 0);
                if (dst==0x0000)
                    Register::SR->bit(dev, STATUS_Z, 1);
                else Register::SR->bit(dev, STATUS_Z, 0);
            }
            this->op2->value(dev, dst);
            Register::SR->bit(dev, STATUS_C, lsb);
            Register::SR->bit(dev, STATUS_N, msb);
            return true;
        }
    };
}
#endif //MSP430EMU_RRC_H

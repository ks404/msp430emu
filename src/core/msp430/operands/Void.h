//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MPS430_VOID_H
#define MSP430EMU_MPS430_VOID_H

#include "../../base/interfaces/AOperand.h"
#include "../../base/exceptions/exceptions.h"

namespace msp430 {
    class Void : public vmc::AOperand {
    public:
        Void() : AOperand(VOID, UNDEF) {}

        // TODO: Add 16-bit mask on value
        uint32_t value(vmc::PDevice dev) const override {
            throw new exc::UnsupportOperationException();
        }

        // TODO: Add 16-bit mask on value
        void value(vmc::PDevice dev, uint32_t data) const override {
            throw new exc::UnsupportOperationException();
        }

        std::string stringify() const override {
            return std::string("");
        }
    };
}

#endif //MSP430EMU_MPS430_VOID_H

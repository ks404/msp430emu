from idc import *

def CreateSegment(name, start, end):
    AddSegEx(start, end, 0x0, False, saRelByte, SEGMOD_SILENT, ADDSEG_QUIET)
    RenameSeg(0x0000, name)
    

CreateSegment("SFR",    0x0000,  0x0010)
CreateSegment("PERI8",  0x0010,  0x0100)
CreateSegment("PERI16", 0x0100,  0x0200)
CreateSegment("RAM",    0x0200,  0x0A00)
CreateSegment("STACK",  0x0A00,  0x0C00)
CreateSegment("BOOT",   0x0C00,  0x1000)
CreateSegment("INFO",   0x1000,  0x1100)
CreateSegment("CODE",   0x1100,  0xFFE0)
CreateSegment("IVT",    0xFFE0, 0x10000)